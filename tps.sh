#!/bin/bash
#thermal process throtteling
VERSION=0.01

UPPER_LIMIT=72
LOWER_LIMIT=71

PID=1845
DEBUG="yes"



while `test -n "\`ps a| grep '^[ ]*'$PID' '\`"`;do
	THERMAL=`cut -f2 </proc/acpi/ibm/thermal | cut -f1 -d " " `
	test "DEBUG" && echo "Temperature: $THERMAL UPPER: $UPPER_LIMIT LOWER: $LOWER_LIMIT"
	test "$THERMAL" -le "$LOWER_LIMIT" && kill -SIGCONT "$PID" && test $DEBUG && echo " wird fortgesetzt"
	test "$THERMAL" -ge "$UPPER_LIMIT" && kill -SIGSTOP "$PID" && test $DEBUG && echo " wird angehalten"
 	sleep 5s
done

